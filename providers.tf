##################################################################################################################
#                                                                                                                #
# The Amazon Web Services (AWS) provider is used to interact with the many resources supported by AWS. The       #
# provider needs to be configured with the proper credentials before it can be used.                             #
#                                                                                                                #
# Further reading: https://registry.terraform.io/providers/hashicorp/aws/latest/docs                             #
#                                                                                                                #
##################################################################################################################

provider "aws" {
  region  = var.region
  profile = var.profile
}
