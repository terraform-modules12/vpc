# AWS VPC Module

### What does it do? 
This module will allow you to create a VPC and required resources in AWS. It will provision the following resource:

 - VPC
 - Internet Gateway
 - NAT Gateway
 - Subnet
 - Public Route Tables
 - Private Route Tables
 - Private Subnet
 - Public Subnet

### Usage

    module  "vpc" {
	    source         =  "git@gitlab.com:terraform-modules12/vpc.git"
    
	    region         =  "eu-west-2"
	    environment    =  "Production"
	    service_name   =  "MyService"
	    vpc_cidr_block =  "10.0.0.0/16"
	    az_limit =  3
    }


### Variable definitions

- region: The region to deploy this VPC to
- environment: The environment this VPC belongs to (development/production)
- service_name: The name of the service this VPC is for (MyWebsite)
- vpc_cidr_block: The cidr range to be used for this VPC
- az_limit: The number of availability zones to use