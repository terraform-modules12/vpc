##################################################################################################################
#                                                                                                                #
# Outputs are a way to tell Terraform what data is important. This data is outputted when apply is called, and   # 
# can be queried using the terraform output command.                                                             #
#                                                                                                                #
# Further reading: https://learn.hashicorp.com/tutorials/terraform/aws-outputs                                   #
#                                                                                                                #
##################################################################################################################
output "account_owner" {
  value = data.aws_caller_identity.current.account_id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpc_cidr_block" {
  value = aws_vpc.vpc.cidr_block
}

output "public_subnet_ids" {
  value = join(",", aws_subnet.subnet_public.*.id)
}

output "private_subnet_ids" {
  value = join(",", aws_subnet.subnet_private.*.id)
}

output "availability_zones" {
  value = join(",", aws_subnet.subnet_private.*.availability_zone)
}
